package de.classes.java;

public class Mutter {
	public String vorname;
	public String nachname;
	
	public void setName(String mutterVorname, String mutterNachname) {
		vorname = mutterVorname;
		nachname = mutterNachname;
	}
	
	public String anzahlKinder() {
		return vorname + " " + nachname + " ist die Mutter der 3 Kinder der Familie Mustermann.";
	}
}

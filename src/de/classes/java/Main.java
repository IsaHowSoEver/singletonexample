package de.classes.java;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Mutter denise = new Mutter();
		denise.setName("Denise", "Mustermann");
		
		Mutter maria = new Mutter();
		maria.setName("Maria", "Mustermann");
		
		System.out.println(denise.anzahlKinder());
		System.out.println(maria.anzahlKinder());
		
		System.out.println("----------------------------------");

//Ausgabe der Singleton Variante
		MutterSingleton deniseSingleton = MutterSingleton.getInstance();		
		deniseSingleton.setName("Denise", "Mustermann");
		
		MutterSingleton mariaSingleton = MutterSingleton.getInstance();		
		mariaSingleton.setName("Maria", "Mustermann");
		
		System.out.println(deniseSingleton.anzahlKinder());
		System.out.println(mariaSingleton.anzahlKinder());
	}

}

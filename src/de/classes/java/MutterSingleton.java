package de.classes.java;

public class MutterSingleton {
	public String vorname;
	public String nachname;
	
//Privater Konstruktor um Objektinstanziierung zu verhindern
	private MutterSingleton() {
		
	}

// Eine privates statisches Objekt der Klasse erzeugt(statisch bewirkt, das immer wieder auf das eube Objekt zugegriffen wird
	private static MutterSingleton mutter = new MutterSingleton();
		
//Liefert das Objekt der MutterSingleton Klasse zurück
	public static MutterSingleton getInstance() {		
		return mutter;
	}
	
	public void setName(String mutterVorname, String mutterNachname) {
		vorname = mutterVorname;
		nachname = mutterNachname;
	}
	
	public String anzahlKinder() {
		return vorname + " " + nachname + " ist die Mutter der 3 Kinder der Familie Mustermann.";
	}

}
